# Generated by Django 4.0.3 on 2023-07-20 19:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='binvo',
            name='closet_name',
            field=models.CharField(default='closet1', max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='shoe',
            name='bin',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='shoe', to='shoes_rest.binvo'),
        ),
    ]
