from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

import json

from common.json import ModelEncoder
from .models import Shoe, BinVO


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href"
    ]


class ListShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin"
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


class DetailShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ListShoeEncoder,
        )
    else:
        content = json.loads(request.body)
        print("objects: ", BinVO.objects.all())
        try:
            bin_id = content["bin"]
            bin_href = f"/api/bins/{bin_id}/"
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Bin does not exist"},
                status=400)

        shoe = Shoe.objects.create(**content)

        return JsonResponse(
            shoe,
            encoder=ListShoeEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_detail_shoes(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=DetailShoeEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Shoe does not exist"},
                status=404)

        shoe.delete()
        return JsonResponse(
            {"message": "Shoe was deleted successfully"},
            status=200)
