import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import HatList from './HatList';

function App(props) {
  if (props.wardrobe) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesList shoes={props.shoes} />} />
          <Route path="/hats" element={<HatList hat={props.hats} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
