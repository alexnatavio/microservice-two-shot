function ShoesList(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model</th>
          <th>Color</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {props.shoes.map(shoe => {
          return (
            <tr key={shoe.id}>
              <td>{ shoe.manufacturer }</td>
              <td>{ shoe.model_name }</td>
              <td>{ shoe.color }</td>
              <td>
                <img src={shoe.picture_url}
                 alt="shoe picture"
                 style={{ maxWidth: '100px', maxHeight: '100px'}} />
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ShoesList;
