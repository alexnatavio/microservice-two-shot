function HatList(props) {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
            {props.hats_rest.map(hat => {
                return (
                    <tr hey={hat.id}>
                        <td>{hat.color}</td>
                        <td>{hat.fabric}</td>
                        <td>{hat.style_name}</td>
                    </tr>
                );
            })}
        </tbody>
      </table>
    );
}

export default HatList;
