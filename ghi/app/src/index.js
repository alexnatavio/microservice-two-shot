import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

async function loadShoes() {
    const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
    if (shoeResponse.ok) {
      const shoeData = await shoeResponse.json();
      console.log(shoeData);
      const root = ReactDOM.createRoot(document.getElementById('root'));
      root.render(
        <React.StrictMode>
          <App shoes={shoeData.shoes} />
        </React.StrictMode>
      );
    }
  }

loadShoes();


async function loadWardrobe() {
  const response = await fetch('http://localhost:8090/api/hats_rest/');
  if (response.ok) {
    const data = await response.json();
    console.log(data);
    const root = ReactDOM.createRoot(document.getElementById('root'));
    root.render(
      <React.StrictMode>
        <App hats={data.hats} />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadWardrobe();
