from django.urls import path
from hats_rest.views import get_hats, manage_hats


urlpatterns = [
    path("hats_rest/", get_hats, name="get_hats"),
    path("hats_rest/<int:id>/", manage_hats, name="manage_hats"),
]
