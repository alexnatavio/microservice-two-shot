from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import HatType, LocationVO


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
    ]


class HatTypeEncoder(ModelEncoder):
    model = HatType
    properties = [
        "id",
        "color",
        "fabric",
        "style_name",
        "picture",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def get_hats(request):

    if request.method == "GET":
        donut = HatType.objects.all()
        print(donut)
        return JsonResponse(
            {"banana": donut},
            encoder=HatTypeEncoder,
        )

    else:
        content = json.loads(request.body)
        location_href = content["location"]
        if location_href:
            try:
                location = LocationVO.objects.get(import_href=location_href)
                content["location"] = location
            except LocationVO.DoesNotExist:
                return JsonResponse(
                    {"message": "invalid location id"},
                    status=400
                )

        donut = HatType.objects.create(**content)
        return JsonResponse(
            donut,
            encoder=HatTypeEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def manage_hats(request, id):

    if request.method == "GET":
        try:
            bin = HatType.objects.get(id=id)
            return JsonResponse(
                bin,
                encoder=HatTypeEncoder,
                safe=False
            )
        except HatType.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            bin = HatType.objects.get(id=id)
            bin.delete()
            return JsonResponse(
                bin,
                encoder=HatTypeEncoder,
                safe=False,
            )
        except HatType.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            bin = HatType.objects.get(id=id)

            props = ["id", "color", "fabric", "style_name", "picture"]
            for prop in props:
                if prop in content:
                    setattr(bin, prop, content[prop])
            bin.save()
            return JsonResponse(
                bin,
                encoder=HatTypeEncoder,
                safe=False,
            )
        except HatType.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
