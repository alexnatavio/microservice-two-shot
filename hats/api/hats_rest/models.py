from django.db import models
from django.db.models import Model


# from django.urls import reverse


class LocationVO(Model):
    import_href = models.CharField(max_length=200, unique=True)


class HatType(models.Model):
    color = models.CharField(max_length=100)
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    picture = models.URLField(max_length=200)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
    )

    # def get_api_url(self):
    #     return reverse("api_bin", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.color} {self.style_name}"

    # class Meta:
    #     ordering = ("location", "color", "fabric", "style_name", "picture")
